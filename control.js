//sudo xboxdrv --ui-buttonmap A=key:KEY_A,B=key:KEY_B,X=key:KEY_X,Y=key:KEY_Y,black=key:KEY_P,white=cycle-key:KEY_1:KEY_2:KEY_3:KEY_4:KEY_5:KEY_6:KEY_7:KEY_8:KEY_9:KEY_0:KEY_F1:KEY_F2:KEY_F3:KEY_F4:KEY_F5:KEY_F6:KEY_F7:KEY_F8:KEY_F9:KEY_F10:KEY_F11:KEY_F12, --silent

var serialport = require("serialport");
var SerialPort = serialport.SerialPort; // localize object constructorr serialport = require("serialport");

// Set a deadzone of +/-3500 (out of +/-32k) and a sensitivty of 350 to reduce signal noise in joystick axis
var joystick = new (require('joystick'))(0, 3500, 350);

var async = require("async");
var sys = require("sys");
var voice = require("./voice.js");

//get the driving module
var drive = require("./driving.js");

//Config options

var verbose = false; //set true to console everything

var dev = "/dev/input/event0"; //input device mount path (xbox controller)

var buttonMap = {};
buttonMap['2'] = "1";
buttonMap['3'] = "2";
buttonMap['4'] = "3";
buttonMap['5'] = "4";
buttonMap['6'] = "5";
buttonMap['7'] = "6";
buttonMap['8'] = "7";
buttonMap['9'] = "8";
buttonMap['10'] = "9";
buttonMap['11'] = "0";
buttonMap['59'] = "F1";
buttonMap['60'] = "F2";
buttonMap['61'] = "F3";
buttonMap['62'] = "F4";
buttonMap['63'] = "F5";
buttonMap['64'] = "F6";
buttonMap['65'] = "F7";
buttonMap['66'] = "F8";
buttonMap['67'] = "F9";
buttonMap['68'] = "F10";
buttonMap['87'] = "F11";
buttonMap['88'] = "F12";
buttonMap['30'] = "a";
buttonMap['48'] = "b";
buttonMap['45'] = "x";
buttonMap['21'] = "y";
buttonMap['25'] = "p";
buttonMap['38'] = "up";
buttonMap['103'] = "up";
buttonMap['39'] = "down";
buttonMap['108'] = "down";
buttonMap['105'] = "left";
buttonMap['106'] = "right";


var soundList = {};
soundList['1'] = "hello_friend";
soundList['2'] = "i_see_you";
soundList['3'] = "coming_through";
soundList['4'] = "target_acquired";
soundList['5'] = "target_lost";
soundList['6'] = "there_you_are";
soundList['7'] = "squishing";
soundList['8'] = "malfunctioning";
soundList['9'] = "put_me_down";
soundList['0'] = "dispense_product";
soundList['F1'] = "hello";
soundList['F2'] = "oh_dear";
soundList['F3'] = "protect_humans";
soundList['F4'] = "activated";
soundList['F5'] = "business_appreciated";
soundList['F6'] = "come_closer";
soundList['F7'] = "dont_hate_you";
soundList['F8'] = "please_stop";
soundList['F9'] = "sentry_mode_activate";
soundList['F10'] = "sorry_dave";
soundList['F11'] = "function";
soundList['F12'] = "kill_all_humans";

//create some state variables

var setSound = 1;

var counter = 0;

var throttle = 0; // 1 = drive, 0 = park, -1 = reverse
var steering = 0; // 1 = right, 0 = straight, -1 = left

function getXbox(device) {
    var xbox = new SerialPort(device, {
        parser: serialport.parsers.raw
    });
    return xbox;
}

try {
    var xbox = getXbox(dev);
} catch (e){
    sys.puts(e.message);
}

xbox.open(function() {
    console.log('open');
    xbox.on('data', function(data) {
        //only do it on the fourth time around
        if ( counter < 3 ){
            counter++;
            return false;
        }
        counter = 0;
        var js = data.toJSON();
        js = js.toString();
		//sys.puts(js);
        var splt = js.split(",");
        var button = splt[10];
		//sys.puts(button);
        button = buttonMap[button];
        //sys.puts("Pressed: " + button);
		//now let's determine what to do with it
		switch (button){
			case "1":
				//audio
				sys.puts(soundList[button]);
				setSound = button;
				break;
            case "2":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "3":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "4":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "5":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "6":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "7":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "8":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "9":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "0":
                //audio
                sys.puts(soundList[button]);
				setSound = button;
                break;
            case "F1":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F2":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F3":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F4":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F5":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F6":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F7":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F8":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F9":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F10":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F11":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
            case "F12":
                //audio
                sys.puts(soundList[button]);
                setSound = button;
                break;
			case "p":
				//actually play audio
				try {
					voice(soundList[setSound]).play();
				} catch (e){
					voice(soundList[setSound]).play();
				}
				break;
			
		}
    });
    xbox.on('end', function(data) {
        sys.puts("end");
    });
});

joystick.on('axis', function(data) {
    var value = 0;
    var number = -1;
    for(var propt in data){
        if ( propt == "number" ){
            number = data[propt];
        } else if ( propt == "value" ){
            value =  data[propt];
        }
    }
    switch (number) {
        case 0:
            //left x-axis
			/*
            if ( value > 0 ){ //right
				if ( verbose ){
	                console.log("left thumb, turn right");
				{
            } else if ( value < 0 ){ //left
				if ( verbose ){
	                console.log("left thumb, turn left");
				}
            } else if ( value == 0 ){
				if ( verbose ){
	                console.log("left thumb, reset");
				}
            } else {
				if ( verbose ){
	                console.log("left thumb, WTF?");
				}
            }
			*/
            break;
        case 1:
            //left y-axis
			var d = drive();
            if ( value > 0 ){ //down
				if ( verbose ){
	                console.log("left thumb, go down");
				}
				if ( throttle == 1 ){ //is driving forward, have to stop first, then reverse
					async.series([
						function(cb){
							d.stop();
							cb(null);
						},
						function(cb){
							d.reverse();
							cb(null);
						}
					],
					function(err, results){
						sys.puts("In reverse");
					});
				} else if ( throttle == 0 ) { // stopped
					d.reverse();
				}
				throttle = -1;
            } else if ( value < 0 ){ //up
				if ( verbose ){
	                console.log("left thumb, go up");
				}
				if ( throttle == -1 ){  //is in reverse, have to stop first
					async.series([
						function(cb){
							d.stop();
							cb(null);
						},
						function(cb){
							d.forward();
							cb(null);
						}
					],
					function(err, results){
						sys.puts("In drive");
					});
				} else if ( throttle == 0 ) { // stopped
					d.forward();
				}
				throttle = 1;
            } else if ( value == 0 ){
				if ( verbose ){
	                console.log("left thumb, reset");
				}
				if ( throttle != 0 ){
					d.stop();
					throttle = 0;
				}
            } else {
				if ( verbose ){
	                console.log("left thumb, WTF?");
				}
            }
            break;
        case 2:
            //right x-axis
			var d = drive();
            if ( value > 0 ){ //right
				if ( verbose ){
	                console.log("right thumb, turn right");
				}
                if ( steering == -1 ){ //is turning left, have to go straight then turn right
                    async.series([
                        function(cb){
                            d.straight();
                            cb(null);
                        },
                        function(cb){
                            d.right();
                            cb(null);
                        }
                    ],
                    function(err, results){
                        sys.puts("Turning right");
                    });
                } else if ( steering == 0 ) { // straight
                    d.right();
                }
                steering = 1;
            } else if ( value < 0 ){ //left
				if ( verbose ){
	                console.log("right thumb, turn left");
				}
                if ( steering == 1 ){  //is turning right, have to go straight then turn left
                    async.series([
                        function(cb){
                            d.straight();
                            cb(null);
                        },
                        function(cb){
                            d.left();
                            cb(null);
                        }
                    ],
                    function(err, results){
                        sys.puts("Turning left");
                    });
                } else if ( steering == 0 ) { // stopped
                    d.left();
                }
                steering = -1;
            } else if ( value == 0 ){
				if ( verbose ){
	                console.log("right thumb, reset");
				}
                if ( steering != 0 ){
                    d.straight();
                    steering = 0;
                }
            } else {
				if ( verbose ){
	                console.log("right thumb, WTF?");
				}
            }
            break;
        case 3:
            //right y-axis
			/*
            if ( value > 0 ){ //down
				if ( verbose ){
	                console.log("right thumb, go down");
				}
            } else if ( value < 0 ){ //up
				if ( verbose ){
	                console.log("right thumb, go up");
				}
            } else if ( value == 0 ){
				if ( verbose ){
	                console.log("right thumb, reset");
				}
            } else {
				if ( verbose ){
	                console.log("right thumb, WTF?");
				}
            }
			*/
            break;
    }
});
