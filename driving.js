//get the gpio module that has been altered to work with piface
//var gpio = require("./pf-gpio");
var sys = require("sys");
var exec = require("child_process").exec;

var controlMap = {};
controlMap['forward'] = "200";
controlMap['reverse'] = "201";
controlMap['right'] = "202";
controlMap['left'] = "203";

//make a function that handles the steering and driving
function drive(){
	return {

		//drive forward
		forward: function forward() {
			sys.puts("Driving forward");
			//interface with the raspberry pi's GPIO, actually the piface's
			exec("gpio -p mode " + controlMap['forward'] + " out", function (error, stdout, stderr) {
				if ( error === null ){
					exec("gpio -p write " + controlMap['forward'] + " 1", function (error2, stdout2, stderr2) {
						if ( error === null ){
							exec("gpio -p mode " + controlMap['forward'] + " in", function (error3, stdout3, stderr3) {
								if ( error !== null ){
									sys.puts(stderr3);
								}
							});
						} else {
							sys.puts(stderr2);
						}
					});
				} else {
					sys.puts(stderr);
				}
			});
		},

		//drive reverse
		reverse: function reverse() {
			sys.puts("Driving reverse");
            exec("gpio -p mode " + controlMap['reverse'] + " out", function (error, stdout, stderr) {
                if ( error === null ){
                    exec("gpio -p write " + controlMap['reverse'] + " 1", function (error2, stdout2, stderr2) {
                        if ( error === null ){
                            exec("gpio -p mode " + controlMap['reverse'] + " in", function (error3, stdout3, stderr3) {
                                if ( error !== null ){
                                    sys.puts(stderr3);
                                }
                            });
                        } else {
                            sys.puts(stderr2);
                        }
                    });
                } else {
                    sys.puts(stderr);
                }
            });
		},

		//stop driving
		stop: function stop() {
			sys.puts("Stop");
            exec("gpio -p mode " + controlMap['forward'] + " out", function (error, stdout, stderr) {
                if ( error === null ){
                    exec("gpio -p write " + controlMap['forward'] + " 0", function (error2, stdout2, stderr2) {
                        if ( error === null ){
                            exec("gpio -p mode " + controlMap['forward'] + " in", function (error3, stdout3, stderr3) {
                                if ( error !== null ){
                                    sys.puts(stderr3);
                                }
                            });
                        } else {
                            sys.puts(stderr2);
                        }
                    });
                } else {
                    sys.puts(stderr);
                }
            });
            exec("gpio -p mode " + controlMap['reverse'] + " out", function (error, stdout, stderr) {
                if ( error === null ){
                    exec("gpio -p write " + controlMap['reverse'] + " 0", function (error2, stdout2, stderr2) {
                        if ( error === null ){
                            exec("gpio -p mode " + controlMap['reverse'] + " in", function (error3, stdout3, stderr3) {
                                if ( error !== null ){
                                    sys.puts(stderr3);
                                }
                            });
                        } else {
                            sys.puts(stderr2);
                        }
                    });
                } else {
                    sys.puts(stderr);
                }
            });
		},

		//turn right
		right: function right() {
			sys.puts("Turn right");
            exec("gpio -p mode " + controlMap['right'] + " out", function (error, stdout, stderr) {
                if ( error === null ){
                    exec("gpio -p write " + controlMap['right'] + " 1", function (error2, stdout2, stderr2) {
                        if ( error === null ){
                            exec("gpio -p mode " + controlMap['right'] + " in", function (error3, stdout3, stderr3) {
                                if ( error !== null ){
                                    sys.puts(stderr3);
                                }
                            });
                        } else {
                            sys.puts(stderr2);
                        }
                    });
                } else {
                    sys.puts(stderr);
                }
            });
		},

        //turn left
        left: function left() {
            sys.puts("Turn left");
            exec("gpio -p mode " + controlMap['left'] + " out", function (error, stdout, stderr) {
                if ( error === null ){
                    exec("gpio -p write " + controlMap['left'] + " 1", function (error2, stdout2, stderr2) {
                        if ( error === null ){
                            exec("gpio -p mode " + controlMap['left'] + " in", function (error3, stdout3, stderr3) {
                                if ( error !== null ){
                                    sys.puts(stderr3);
                                }
                            });
                        } else {
                            sys.puts(stderr2);
                        }
                    });
                } else {
                    sys.puts(stderr);
                }
            });
        },

        //steer straight
        straight: function straight() {
            sys.puts("Straight");
            exec("gpio -p mode " + controlMap['right'] + " out", function (error, stdout, stderr) {
                if ( error === null ){
                    exec("gpio -p write " + controlMap['right'] + " 0", function (error2, stdout2, stderr2) {
                        if ( error === null ){
                            exec("gpio -p mode " + controlMap['right'] + " in", function (error3, stdout3, stderr3) {
                                if ( error !== null ){
                                    sys.puts(stderr3);
                                }
                            });
                        } else {
                            sys.puts(stderr2);
                        }
                    });
                } else {
                    sys.puts(stderr);
                }
            });
            exec("gpio -p mode " + controlMap['left'] + " out", function (error, stdout, stderr) {
                if ( error === null ){
                    exec("gpio -p write " + controlMap['left'] + " 0", function (error2, stdout2, stderr2) {
                        if ( error === null ){
                            exec("gpio -p mode " + controlMap['left'] + " in", function (error3, stdout3, stderr3) {
                                if ( error !== null ){
                                    sys.puts(stderr3);
                                }
                            });
                        } else {
                            sys.puts(stderr2);
                        }
                    });
                } else {
                    sys.puts(stderr);
                }
            });	
		},
	};
}

module.exports = drive;
